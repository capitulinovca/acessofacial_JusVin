CREATE TABLE `Pessoa` (
	`idPessoa` INT NOT NULL AUTO_INCREMENT,
	`NomeCompleto` varchar(60) NOT NULL UNIQUE,
	`CPF` INT NOT NULL UNIQUE,
	`Matricula` INT NOT NULL UNIQUE,
	`CursoFk` INT NOT NULL,
	`Semestre` INT NOT NULL,
	`PessoaTipo` INT NOT NULL,
	PRIMARY KEY (`idPessoa`)
);

CREATE TABLE `Disciplina` (
	`idDisciplina` INT NOT NULL AUTO_INCREMENT,
	`Nome` varchar(50) NOT NULL,
	`Turma` varchar(10) NOT NULL,
	PRIMARY KEY (`idDisciplina`)
);

CREATE TABLE `Curso` (
	`idCurso` INT NOT NULL AUTO_INCREMENT,
	`Nome` varchar(50) NOT NULL UNIQUE,
	`NivelFk` INT NOT NULL,
	PRIMARY KEY (`idCurso`)
);

CREATE TABLE `CursoDisciplina` (
	`CursoFk` INT NOT NULL,
	`DisciplinaFk` INT NOT NULL,
	`Semestre` INT NOT NULL
);

CREATE TABLE `Nivel` (
	`idNivel` INT NOT NULL AUTO_INCREMENT,
	`Nome` varchar(50) NOT NULL,
	PRIMARY KEY (`idNivel`)
);

CREATE TABLE `Laboratórios` (
	`idLab` INT NOT NULL AUTO_INCREMENT,
	`Nome` varchar(50) NOT NULL,
	PRIMARY KEY (`idLab`)
);

CREATE TABLE `DisciplinaHorarios` (
	`DisciplinaFk` INT NOT NULL,
	`HorarioFk` INT NOT NULL,
	`DiaSemanaFk` INT NOT NULL,
	`LaboratorioFk` INT NOT NULL,
	`Horarios` DATETIME NOT NULL
);

CREATE TABLE `DiasSemana` (
	`idDia` INT NOT NULL AUTO_INCREMENT,
	`DiaSemana` INT NOT NULL,
	PRIMARY KEY (`idDia`)
);

CREATE TABLE `PessoaTipo` (
	`idTipo` INT NOT NULL AUTO_INCREMENT,
	`PessoaTipo` INT NOT NULL,
	PRIMARY KEY (`idTipo`)
);

ALTER TABLE `Pessoa` ADD CONSTRAINT `Pessoa_fk0` FOREIGN KEY (`CursoFk`) REFERENCES `Curso`(`idCurso`);

ALTER TABLE `Pessoa` ADD CONSTRAINT `Pessoa_fk1` FOREIGN KEY (`PessoaTipo`) REFERENCES `PessoaTipo`(`idTipo`);

ALTER TABLE `Curso` ADD CONSTRAINT `Curso_fk0` FOREIGN KEY (`NivelFk`) REFERENCES `Nivel`(`idNivel`);

ALTER TABLE `CursoDisciplina` ADD CONSTRAINT `CursoDisciplina_fk0` FOREIGN KEY (`CursoFk`) REFERENCES `Curso`(`idCurso`);

ALTER TABLE `CursoDisciplina` ADD CONSTRAINT `CursoDisciplina_fk1` FOREIGN KEY (`DisciplinaFk`) REFERENCES `Disciplina`(`idDisciplina`);

ALTER TABLE `DisciplinaHorarios` ADD CONSTRAINT `DisciplinaHorarios_fk0` FOREIGN KEY (`DisciplinaFk`) REFERENCES `Disciplina`(`idDisciplina`);

ALTER TABLE `DisciplinaHorarios` ADD CONSTRAINT `DisciplinaHorarios_fk1` FOREIGN KEY (`HorarioFk`) REFERENCES `Horarios`(`idHorario`);

ALTER TABLE `DisciplinaHorarios` ADD CONSTRAINT `DisciplinaHorarios_fk2` FOREIGN KEY (`DiaSemanaFk`) REFERENCES `DiasSemana`(`idDia`);

ALTER TABLE `DisciplinaHorarios` ADD CONSTRAINT `DisciplinaHorarios_fk3` FOREIGN KEY (`LaboratorioFk`) REFERENCES `Laboratórios`(`idLab`);

