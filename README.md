# Sistema de controle de acesso
Sistema criado para controlar o acesso de pessoas a ambientes previamente cadastrados.  

**Funcionalidades:**
- Cadastro de Usuario  
- Cadastro de Salas  
- Cadastro de Cursos  
- Cadastro de Disciplinas  
- Pesquisa de Salas Disponiveis por Data e Horarios  
- Pesquisa de Usuarios  
- Verificacao de Permissao de Acesso  
- Reconhecimento Facial  



### Código Fonte  

**jusvin.cpp**  
Implementa as funções usadas no programa.  
**jusvin.hpp**  
Contém os protótipos de todas as funções usadas.  
**main.cpp**  
É a interface de chamada dos métodos do menu   

### Compilando o código  
> _g++ -ojusvin  jusvin.cpp main.cpp jusvin.h_


### Paradigma Orientado ao Objeto e a linguagem de programação usada  
Este programa foi implementado em C++ usando o paradigma orientado ao objeto.  
**Classes**
  >Usuario  
  >> Aluno  
  >> Professor  
  >> Funcionario Terceirizado  
  >> Suporte Tecnico CIC  
  >> Prestador de Serviço  

  >Disciplina  
  >Curso  
  >Horario  
  >Sala  
  >Menu

O sistema também precisa cadastrar reservas, que podem ser para aulas de disciplinas e também podem ser horários para limpeza, manutenção ou mesmo reservas individuais, por exemplo, para alguma reunião esporádica ou uma sessão preparatória para maratona de programação. Cada reserva possui ao menos essas informações:

Autor da reserva, identificado por um número (ex. matricula);
Propósito da reserva (um string com o título, ex. nome da disciplina);
Se for para aula, informe o código da disciplina e da turma;
Número da(s) sala(s) onde o evento ocorrerá;
Horários;
Informe se é uma reserva recorrente (ou seja, toda semana, toda terça e quinta). Se for recorrente, informe a primeira e a última data da reserva.
Será necessário armazenar um vetor indicando o número de matrícula de todos que participarão do evento (por exemplo, todos alunos, professores e monitores de uma disciplina, ou todos os funcionários da limpeza que estarão lá naquele horário). Isso pode ser como um atributo da classe de reserva ou pode ser armazenado em outra classe ou estrutura de dados.

Primeira versão da modelagem do sistema:

![alt text](https://cdn.pbrd.co/images/1ZNZgqNNv.png)

Versão final da modelagem do sistema:
![alt text](https://cdn.pbrd.co/images/20mq1JMhv.png)


  Departamento de Ciência da Computação - CIC  
  Disciplina: Tecnicas de Programação 1  
  Professora: Teófilo E. de Campos  
  Código: 117889 – Período Letivo: 1/2017  
  Aluno: Vinicius Capitulino de Andrade  Matrícula: 150151233  
  Aluno: Juscelino Diógenes Santana      Matrícula: 150154046
