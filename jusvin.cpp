/*! @file: jusvin.cpp
 *  @author: Vinicius Capitulino and Juscelino Santana
 *  @brief: Implementa as funções usadas no código
 */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string.h>
#include <cstring>
#include "jusvin.hpp"

using namespace std;



//~~~~~~~~~~~~~~METODOS DA CLASSE DISCIPLINA~~~~~~~~~~~~~~~~~~~~~~~~//

bool Disciplina::set_departamento(char novo_departamento[]){
  if(strlen(novo_departamento)<20){
    strcpy (departamento,novo_departamento);
    return true;
  }else{
    cout << "Erro!!!" << endl;
    return false;
  }
}

bool Disciplina::set_nomeDisc(char novo_nomeDisc[]){
  if(strlen(novo_nomeDisc)<20){
    strcpy (nomeDisc,novo_nomeDisc);
    return true;
  }else{
    cout << "Erro!!!" << endl;
    return false;
  }
}

//! Função que recebe o novo nome para o cadastro
    /*!
      \param novo_nome nome inserido pelo usuario
      \return True ou False como verificação para a rotina
    */
bool Usuario::set_nome(char novo_nome[]){
      if(strlen(novo_nome)<20){
        strcpy (nome,novo_nome);
        return true;
    }else{
      cout << "Erro!!!" << endl;
      return false;
    }
}

bool Usuario::set_sobrenome(char novo_sobrenome[]){
  if(strlen(novo_sobrenome)<20){
    strcpy (sobrenome,novo_sobrenome);
    return true;
  }else{
    cout << "Erro!!!" << endl;
    return false;
  }
}

bool Usuario::set_cpf(char novo_cpf[]){
  if(strlen(novo_cpf)==11){
    strcpy (cpf,novo_cpf);
    return true;
  }else{
    cout << "Erro!!!" << endl;
    return false;
  }
}

bool Usuario::set_matricula(char nova_matricula[]){
  if(strlen(nova_matricula)<15){
    strcpy (matricula,nova_matricula);
    return true;
  }else{
    cout << "Erro!!!" << endl;
    return false;
  }
}

bool Usuario::set_empresa(char nova_empresa[]){
  if(strlen(nova_empresa)<50){
    strcpy (empresa,nova_empresa);
    return true;
  }else{
    cout << "Erro!!!" << endl;
    return false;
  }
}

bool Usuario::set_setor(char novo_setor[]){
  if(strlen(novo_setor)<20){
    strcpy (setor,novo_setor);
    return true;
  }else{
    cout << "Erro!!!" << endl;
    return false;
  }
}


void Usuario::gravaArq(Usuario novo_usuario) {
  char nomeArq[30];
//  char* exten = ".txt";
  cout << "Nome do arquivo:";
  cin >> nomeArq;
//  strcat(nomeArq,exten);
//  cout << "O nome do arquivo é:" << nomeArq << endl;
  ofstream fgrava(nomeArq);
  fgrava.write ((char*)&novo_usuario, sizeof(Usuario));
}

void Usuario::leArq (Usuario novo_usuario) {
  char nomeArq[30];
  //char* exten = ".txt";
  cout << "Arquivo para leitura###:";
  cin >> nomeArq;
  ifstream fread(nomeArq);
  fread.read ((char*)&novo_usuario, sizeof(Usuario));

}

void Usuario::cadastro (){
  system ("clear");
  cout<<"#######################################################"<< endl;
  cout<<"                   CADASTRO DE USUARIO                 "<< endl;
  cout<<"#######################################################"<< endl << endl << endl;

  cout<<"Nome:";
  char nome[20];
  cin>> nome;
  Usuario::set_nome(nome);

  cout<<"Sobrenome:";
  char sobrenome[20];
  cin>> sobrenome;
  Usuario::set_nome(sobrenome);

  cout<<"CPF:";
  char cpf[11];
  cin>> cpf;
  Usuario::set_cpf(cpf);

  system ("clear");
  cout<<"#######################################################"<< endl;
  cout<<"                   CADASTRO DE USUARIO                 "<< endl;
  cout<<"#######################################################"<< endl << endl << endl;

  cout << "Escolha uma categoria:" << endl;
  cout << "1 - Aluno" << endl;
  cout << "2 - Professor" << endl;
  cout << "3 - Funcionario Terceirizado" << endl;
  cout << "4 - Suporte Tecnico CIC" << endl;
  cout << "5 - Prestador de Serviço" << endl;

  int z;
  cin >> z;
//SE ADICIONAR OPÇÃO NO MENU, MUDE A CONDIÇÃO DO WHILE
  while(z>5){
    cout<<"Número inválido."<< endl << "Digite uma opção valida:";
    cin >> z;
  }

  switch (z) {
    case 1:
      system ("clear");
          cout<<"#######################################################"<< endl;
          cout<<"                   CADASTRO DE ALUNO                 "<< endl;
          cout<<"#######################################################"<< endl << endl << endl;

          cout<<"Curso:";
          char nomeCurso[100];
          cin>>nomeCurso;
          Usuario::set_nomeCurso(nomeCurso);

          char mat[15];
          cout<<"Matricula:";
          cin>>mat;
          Usuario::set_matricula(mat);

          //chamada do método que salvar no banco de dados
          break;
    case 2:
    system ("clear");
        cout<<"#######################################################"<< endl;
        cout<<"                   CADASTRO DE PROFESSOR                 "<< endl;
        cout<<"#######################################################"<< endl << endl << endl;

        cout<<"Matricula:";
        cin>>mat;
        Usuario::set_matricula(mat);

        cout<<"Departamento:";
        char departamento[20];
        cin>>departamento;
        Disciplina::set_departamento(departamento);

    break;

  };
}

void Usuario::verifica() {
  system ("clear");
  cout<<"#######################################################"<<'\n';
  cout<<"                      VERIFICACAO                      "<<'\n';
  cout<<"#######################################################\n\n\n";
  cout<<"            ######## EM CONSTRUÇÃO ########             "<<endl;

};

//~~~~~~~~~~~~~~ FIM METODOS DA CLASSE USUARIO~~~~~~~~~~~~~~~~~~~~~~~~//




//~~~~~~~~~~~~~~METODOS DA CLASSE DISCIPLINA~~~~~~~~~~~~~~~~~~~~~~~~//
bool Curso::set_nomeCurso(char novo_nome[]){
  if(strlen(novo_nome)<100){
    strcpy(nomeCurso,novo_nome);
    return true;
  }else{
    cout << "Erro!!!" << endl;
    return false;
  }
}

bool Disciplina::set_cod(char novo_cod[]){
    if(strlen(novo_cod)<10){
      strcpy(codigo,novo_cod);
      return true;
    }else{
      cout << "Erro!!!" << endl;
      return false;
    }
}

bool Curso::set_departamento(char novo_dep[]){
    if(strlen(novo_dep)<100){
      strcpy(departamento,novo_dep);
      return true;
    }else{
      cout << "Erro!!!" << endl;
      return false;
    }
}

void Disciplina::get_nome(char nome[]){
      strcpy(this->nome,nome);
}

void Disciplina::get_cod(char codigo[]){
      strcpy(this->codigo,codigo);
}

void Curso::get_dep(char departamento[]){
      strcpy(this->departamento,departamento);
}



void Disciplina::cadastro (){
  system ("clear");
  cout<<"#######################################################"<< endl;
  cout<<"                CADASTRO DE DISCIPLINA                 "<< endl;
  cout<<"#######################################################"<< endl << endl << endl;

  cout<<"Nome da disciplina:";
  char nomeDisc[100];
  cin>> nomeDisc;
  Disciplina::set_nomeDisc(nomeDisc);

  cout<<"Código da Disciplina:";
  char codDisc[10];
  cin >> codDisc;
  Disciplina::set_cod(codDisc);

  cout << "Departamento:";
  char dep[100];
  cin >> dep;
  Disciplina::set_departamento(dep);

  //Adicionar código para manipular arquivo de texto

};

//~~~~~~~~~~~~~~ FIM METODOS DA CLASSE DISCIPLINA~~~~~~~~~~~~~~~~~~~~~~~~//

//~~~~~~~~~~~~~~METODOS CLASSE MENU~~~~~~~~~~~~~~~~~~~//
void Menu::encerra(){
  system ("clear");
  cout<<"#######################################################"<< endl;
  cout<<"                      FIM DO PROGRAMA                  "<< endl;
  cout<<"#######################################################"<< endl;
  exit(0);
};

void Menu::menuCadastro(){
  Usuario novo_usuario; // criada uma instancia de usuario
  novo_usuario.cadastro();
  novo_usuario.gravaArq(novo_usuario);
    Menu::menuPrinc();
};

void Menu::menuVerifica(){
  system ("clear");
  cout<<"#######################################################"<< endl;
  cout<<"                  VERIFICACAO DE USUARIO               "<< endl;
  cout<<"#######################################################"<< endl;
  //novo_usuario.verifica();
  Usuario novo_usuario;
  novo_usuario.leArq(novo_usuario);

//  cin.get();
  //cin.get();
  //Menu::menuPrinc();
};

void Menu::menuBaseDados(){
  system ("clear");
  cout<<"#######################################################"<< endl;
  cout<<"             CONFIGURAÇÕES DA BASE DE DADOS            "<< endl;
  cout<<"#######################################################"<< endl;
  cout<<"1- Cadastro de Disciplina"<< endl;
  cout<<"2- Cadastro de Sala"<< endl;
  cout<<"3- Voltar para o Menu Principal"<< endl;
  cout<<"4- Encerrar o Programa"<< endl;

  int z;
  cin >> z;
//SE ADICIONAR OPÇÃO NO MENU, MUDE A CONDIÇÃO DO WHILE
  while(z>4){
    cout<<"Número inválido."<< endl << "Digite uma opção valida:";
    cin >> z;
  }
  switch (z) {
    case 1:
      Disciplina nova_disciplina;
      nova_disciplina.cadastro();
  //  nova_disciplina.display();
      Menu::menuPrinc();
    break;

    case 2:
      system ("clear");
      cout<<" ######## EM CONSTRUÇÃO ########"<<endl;
      cin.get();
      cin.get();
      Menu::menuPrinc();
    break;

    case 3:
      Menu::menuPrinc();
    break;

    case 4:
      Menu::encerra();
    break;
  }
};

void Menu::menuPrinc(){
  system ("clear");
  //system("Jusvin - Controle de Acesso");
  cout<<"#######################################################"<< endl;
  cout<<"                      JUSVIN                           "<< endl;
  cout<<"#######################################################"<< endl << endl << endl;
  cout<<"Digite:"<< endl;
  cout<<"1- Cadastro de Usuarios"<< endl;
  cout<<"2- Verificacao de Acesso"<< endl;
  cout<<"3- Base de dados"<< endl;
  cout<<"4- Sair"<< endl;
  cout<<"Opção desejada:";

  int x;
  cin >> x;

//SE ADICIONAR OPÇÃO NO MENU, MUDE A CONDIÇÃO DO WHILE
while(x>4){
  cout<<"Número inválido."<< endl << "Digite uma opção valida:";
  cin >> x;
}

  if(x==4){
    Menu::encerra();
  }else
      switch (x) {
        case 1:
          Menu::menuCadastro();

         break;

        case 2:
          Menu::menuVerifica();
          cin.get();
          cin.get();
          Menu::menuPrinc();
        break;

       case 3:
          Menu::menuBaseDados();
       break;
     }


};
