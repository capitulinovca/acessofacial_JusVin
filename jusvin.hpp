/*! @file: jusvin.h
 *  @brief: Arquivo Header de jusvin. Aqui todos os prototipos sao apresentados
 *  @author: Vinicius Capitulino e Juscelino Santana
 */

 #ifndef __jusvin__
 #define __jusvin__


 //~~~~~~~~~~~~~~~INICIO CLASSE CURSO~~~~~~~~~~~~~~~~~~~~~~~~~~//

 class Curso {

   public:
     bool set_nomeCurso(char nomeCurso[]);
     bool set_departamento(char departamento[]);
     void get_dep(char departamento[]);

   private:
     char nomeCurso[100];
     char departamento[20];
 };
 //~~~~~~~~~~~~~~~~FIM CLASSE CURSO~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~ĨNICIO DA CLASSE DISCIPLINA~~~~~~~~~~~~~~~~~~~~~~~~//

//!  Classe Disciplina
 /*!
   Modela as disciplinas, inserindo o departamento, as turmas, horarios e salas usadas.
 */
class Disciplina : public Curso {

public:
//SETTERS
  bool set_nomeDisc(char novo_nomeDisc[]);
  bool set_cod(char novo_cod[]);
  bool set_departamento(char novo_departamento[]);
  void cadastro();
  //void gravaArq(Usuario novo_usuario);
  //void leArq(Usuario novo_usuario);

//GETTERS
  void get_nome(char nome[]);
  void get_cod(char codigo[]);


//  void display();


private:
  char nome[100];
  char codigo[10];
  char departamento[20];
  char nomeDisc[20];

};
//~~~~~~~~~~~~~~~~FIM CLASSE DISCIPLINA~~~~~~~~~~~~~~~~~~~~~~~//






//~~~~~~~~~~~~~~~INICIO CLASSE HORARIO~~~~~~~~~~~~~~~~~~~~~~~~~~//
//class Horario: {

//}
//~~~~~~~~~~~~~~~~FIM CLASSE HORARIO~~~~~~~~~~~~~~~~~~~~~~~~~~~//



//~~~~~~~~~~~~~~~INICIO CLASSE SALA~~~~~~~~~~~~~~~~~~~~~~~~~//

//class Sala: public Horario{
/*
O sistema também precisa cadastrar reservas, que podem ser para aulas de disciplinas e também podem ser horários para limpeza, manutenção ou mesmo reservas individuais, por exemplo, para alguma reunião esporádica ou uma sessão preparatória para maratona de programação. Cada reserva possui ao menos essas informações:

Autor da reserva, identificado por um número (ex. matricula);
Propósito da reserva (um string com o título, ex. nome da disciplina);
Se for para aula, informe o código da disciplina e da turma;
Número da(s) sala(s) onde o evento ocorrerá;
Horários;
Informe se é uma reserva recorrente (ou seja, toda semana, toda terça e quinta). Se for recorrente, informe a primeira e a última data da reserva.
Será necessário armazenar um vetor indicando o número de matrícula de todos que participarão do evento (por exemplo, todos alunos, professores e monitores de uma disciplina, ou todos os funcionários da limpeza que estarão lá naquele horário). Isso pode ser como um atributo da classe de reserva ou pode ser armazenado em outra classe ou estrutura de dados.
*/
//}
//~~~~~~~~~~~~~~~~FIM CLASSE SALA~~~~~~~~~~~~~~~~~~~~~~~~~~//


//~~~~~~~~~~~~~~~INICIO CLASSE USUARIO~~~~~~~~~~~~~~~~~~~~~~~~~//
//!  Classe Usuario
/*!
  Modela os Usuarios.
*/

class Usuario : public Disciplina
{

public:
  void cadastro ();/*!< Método que cadastra todos os usuarios */
  void verifica ();/*!< Método que verifica e valida o acesso para usuarios cadastrados */

//SETTERS
  bool set_nome (char novo_nome[]);
  bool set_sobrenome(char novo_sobrenome[]);
  bool set_cpf(char novo_cpf[]);
  bool set_matricula(char nova_matricula[15]);
  bool set_empresa(char nova_empresa[50]);
  bool set_setor(char novo_setor[20]);
  void leArq(Usuario novo_usuario);
  void gravaArq(Usuario novo_usuario);
//GETTERS


private:
  char nome[20];
  char sobrenome[20];
  char cpf[11];
  char matricula[15];
  char empresa[50];
  char setor[20];

};
//~~~~~~~~~~~~~~~~~FIM CLASSE USUARIO~~~~~~~~~~~~~~~~~~~~~~~~//





//~~~~~~~~~~~~~~~~ĨNICIO CLASSE MENU~~~~~~~~~~~~~~~~~//
class Menu {
public:
  void menuPrinc();
  void menuCadastro();
  void menuVerifica();
  void menuBaseDados();
  void encerra();

};
//~~~~~~~~~~~~~~~~~FIM CLASSE USUARIO~~~~~~~~~~~~~~~~~~~~~~~~//

#endif
